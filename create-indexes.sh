#!/bin/bash

outdir='/mnt/luiz/experiments/vary-parameters'
indexes=('trees' 'expanded')
exec='./tgraph-disk-manager/tgraph-disk-manager'

mkdir -p $outdir

for pn in `seq 2 9 `; do
    for ptau in `seq 2 9 `; do
        for index in ${indexes[@]}; do
            for i in `seq 10`; do
                n=$((2**$pn))
                tau=$((2**$ptau))
                file="$index-$n-$tau-$i"
                eval "$exec -o create -i $index -n $n -t $tau $file"
                mv $file "$outdir/$file"
            done
        done
    done
done
